<?php


namespace Apteka\MetricsExtension\Routing;


use Apteka\ApArray;
use Apteka\Routing\RoutesProviderInterface;
use Apteka\Routing\Routing;

class MetricsPrimaryRoutesProvider implements RoutesProviderInterface
{
    /**
     * @var string
     */
    private $extensionPath;


    /**
     * @param string $extensionPath
     */
    public function __construct($extensionPath)
    {
        $this->extensionPath = $extensionPath;
    }

    /**
     * @inheritdoc
     */
    public function getRoutes()
    {

        $controllersPath = "$this->extensionPath/Resources/controllers";

        return [
            [
                'pattern' => '%^/metrics/%',
                'controller' => "$controllersPath/metrics/index.php",
                'controller_absolute_path' => true,
                'is_primary' => true,
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getPriority()
    {
        return 100;
    }
}