<?php

namespace Apteka\MetricsExtension\Prometheus;

use Apteka\Db\Connection;
use Prometheus\CollectorRegistry as PrometheusCollectorRegistry;

abstract class AbstractPrometheusMetricsDbc extends AbstractPrometheusMetrics
{
    /**
     * @var Connection
     */
    protected $dbc;

    /**
     * @param PrometheusCollectorRegistry $prometheusCollectorRegistry
     * @param Connection $dbc
     */
    public function __construct(
        PrometheusCollectorRegistry $prometheusCollectorRegistry,
        Connection $dbc
    ) {
        parent::__construct($prometheusCollectorRegistry);
        $this->dbc = $dbc;
    }
}