<?php

namespace Apteka\MetricsExtension\Prometheus;

use Apteka\MetricsExtension\MetricsInterface;
use Prometheus\CollectorRegistry as PrometheusCollectorRegistry;
use Prometheus\Counter;
use Prometheus\Storage\Adapter;

abstract class AbstractPrometheusMetrics implements MetricsInterface
{
    /**
     * @var PrometheusCollectorRegistry
     */
    protected $prometheusCollectorRegistry;

    /**
     * @param PrometheusCollectorRegistry $prometheusCollectorRegistry
     */
    public function __construct(
        PrometheusCollectorRegistry $prometheusCollectorRegistry
    ) {
        $this->prometheusCollectorRegistry = $prometheusCollectorRegistry;
    }

    /**
     * @return string
     */
    public function getNameSpace()
    {
        return 'apteka';
    }
}