<?php

namespace Apteka\MetricsExtension\Prometheus;

use Exception;;
use Prometheus\Counter;
use Prometheus\Gauge;
use Prometheus\Histogram;
use Prometheus\MetricFamilySamples;
use Prometheus\Storage\Adapter;
use Predis\Client;
use Prometheus\Storage\Redis;
use Psr\Log\LoggerInterface;

/**
 * @see Redis Построен на основе этого класса путем замены подключения на predis
 */

class PrometheusRedisAdapter implements Adapter
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /*
     * @var string
     */
    const PREFIX = 'PROMETHEUS_';

    /*
     * @var Client
     */
    private $redis;

    /**
     * @param Client $client
     * @param LoggerInterface $logger
     */
    public function __construct(Client $client, LoggerInterface $logger)
    {
        $this->redis = $client;
        $this->logger = $logger;
    }

    /**
     * @return MetricFamilySamples[]
     */
    public function collect()
    {
        $metrics = $this->collectHistograms();
        $metrics = array_merge($metrics, $this->collectGauges());
        $metrics = array_merge($metrics, $this->collectCounters());
        return array_map(
            function (array $metric) {
                return new MetricFamilySamples($metric);
            },
            $metrics
        );
    }

    public function updateHistogram(array $data)
    {
        $bucketToIncrease = '+Inf';
        foreach ($data['buckets'] as $bucket) {
            if ($data['value'] <= $bucket) {
                $bucketToIncrease = $bucket;
                break;
            }
        }
        $metaData = $data;
        unset($metaData['value']);
        unset($metaData['labelValues']);

        $this->redisCatchingErrors(
            function() use ($data, $metaData, $bucketToIncrease) {
                $this->redis->eval(<<<LUA
    local increment = redis.call('hIncrByFloat', KEYS[1], ARGV[1], ARGV[3])
    redis.call('hIncrBy', KEYS[1], ARGV[2], 1)
    if increment == ARGV[3] then
        redis.call('hSet', KEYS[1], '__meta', ARGV[4])
        redis.call('sAdd', KEYS[2], ARGV[5])
    end

LUA
                    ,
                    2,
                    $this->addPrefix($this->toMetricKey($data)),
                    $this->addPrefix(Histogram::TYPE),

                    json_encode(['b' => 'sum', 'labelValues' => $data['labelValues']]),
                    json_encode(['b' => $bucketToIncrease, 'labelValues' => $data['labelValues']]),
                    $data['value'],
                    json_encode($metaData),
                    $this->toMetricKey($data)
                );
            }
        );
    }

    public function updateGauge(array $data)
    {
        $metaData = $data;

        unset($metaData['value']);
        unset($metaData['labelValues']);
        unset($metaData['command']);
        $this->redisCatchingErrors(
            function() use ($data, $metaData) {
                $this->redis->eval(<<<LUA
local result = redis.call(ARGV[1], KEYS[1], ARGV[2], ARGV[3])

if ARGV[1] == 'hSet' then
    if result == 1 then
        redis.call('hSet', KEYS[1], '__meta', ARGV[4])
        redis.call('sAdd', KEYS[2], ARGV[5])
    end
else
    if result == ARGV[3] then
        redis.call('hSet', KEYS[1], '__meta', ARGV[4])
        redis.call('sAdd', KEYS[2], ARGV[5])
    end
end
LUA
                    ,
                    2,
                    $this->addPrefix($this->toMetricKey($data)),
                    $this->addPrefix(Gauge::TYPE),

                    $this->getRedisCommand($data['command']),
                    json_encode($data['labelValues']),
                    $data['value'],
                    json_encode($metaData),
                    $this->toMetricKey($data)
                );
            }
        );
    }

    public function updateCounter(array $data)
    {
        $metaData = $data;
        unset($metaData['value']);
        unset($metaData['labelValues']);
        unset($metaData['command']);

        $result = $this->redisCatchingErrors(
            function() use ($data, $metaData) {
                return $this->redis->eval(<<<LUA
local result = redis.call(ARGV[4], KEYS[1], ARGV[1], ARGV[2])
if result == tonumber(ARGV[2]) then
    redis.call('hMSet', KEYS[1], '__meta', ARGV[3])
    redis.call('sAdd', KEYS[2], ARGV[5])
end

return result

LUA
                        ,
                        2,
                        $this->addPrefix($this->toMetricKey($data)),
                        $this->addPrefix(Counter::TYPE),

                        json_encode($data['labelValues']),
                        $data['value'],
                        json_encode($metaData),
                        $this->getRedisCommand($data['command']),
                        $this->toMetricKey($data)

                );
            }
        );
        return $result;
    }

    private function collectHistograms()
    {
        $keys = $this->redisCatchingErrors(
            function() {
                return $this->redis->smembers($this->addPrefix(Histogram::TYPE));
            }
        );
        sort($keys);
        $histograms = array();
        foreach ($keys as $key) {
            $raw = $this->redisCatchingErrors(
                function() use ($key) {
                    return $this->redis->hgetall($this->addPrefix($key));
                }
            );
            $histogram = json_decode($raw['__meta'], true);
            unset($raw['__meta']);
            $histogram['samples'] = array();

            // Add the Inf bucket so we can compute it later on
            $histogram['buckets'][] = '+Inf';

            $allLabelValues = array();
            foreach (array_keys($raw) as $k) {
                $d = json_decode($k, true);
                if ($d['b'] == 'sum') {
                    continue;
                }
                $allLabelValues[] = $d['labelValues'];
            }

            // We need set semantics.
            // This is the equivalent of array_unique but for arrays of arrays.
            $allLabelValues = array_map("unserialize", array_unique(array_map("serialize", $allLabelValues)));
            sort($allLabelValues);

            foreach ($allLabelValues as $labelValues) {
                // Fill up all buckets.
                // If the bucket doesn't exist fill in values from
                // the previous one.
                $acc = 0;
                foreach ($histogram['buckets'] as $bucket) {
                    $bucketKey = json_encode(array('b' => $bucket, 'labelValues' => $labelValues));
                    if (!isset($raw[$bucketKey])) {
                        $histogram['samples'][] = array(
                            'name' => $histogram['name'] . '_bucket',
                            'labelNames' => array('le'),
                            'labelValues' => array_merge($labelValues, array($bucket)),
                            'value' => $acc
                        );
                    } else {
                        $acc += $raw[$bucketKey];
                        $histogram['samples'][] = array(
                            'name' => $histogram['name'] . '_bucket',
                            'labelNames' => array('le'),
                            'labelValues' => array_merge($labelValues, array($bucket)),
                            'value' => $acc
                        );
                    }
                }

                // Add the count
                $histogram['samples'][] = array(
                    'name' => $histogram['name'] . '_count',
                    'labelNames' => array(),
                    'labelValues' => $labelValues,
                    'value' => $acc
                );

                // Add the sum
                $histogram['samples'][] = array(
                    'name' => $histogram['name'] . '_sum',
                    'labelNames' => array(),
                    'labelValues' => $labelValues,
                    'value' => $raw[json_encode(array('b' => 'sum', 'labelValues' => $labelValues))]
                );
            }
            $histograms[] = $histogram;
        }
        return $histograms;
    }

    private function collectGauges()
    {
        $keys = $this->redisCatchingErrors(
            function() {
                return $this->redis->smembers($this->addPrefix(Gauge::TYPE));
            }
        );

        sort($keys);
        $gauges = array();
        foreach ($keys as $key) {
            $raw = $this->redisCatchingErrors(
                function() use ($key) {
                    return $this->redis->hgetall($this->addPrefix($key));
                }
            );
            $gauge = json_decode($raw['__meta'], true);
            unset($raw['__meta']);
            $gauge['samples'] = array();
            foreach ($raw as $k => $value) {
                $gauge['samples'][] = array(
                    'name' => $gauge['name'],
                    'labelNames' => array(),
                    'labelValues' => json_decode($k, true),
                    'value' => $value
                );
            }
            usort($gauge['samples'], function($a, $b){
                return strcmp(implode("", $a['labelValues']), implode("", $b['labelValues']));
            });
            $gauges[] = $gauge;
        }
        return $gauges;
    }

    private function collectCounters()
    {
        $keys = $this->redisCatchingErrors(
            function() {
                 return $this->redis->smembers($this->addPrefix(Counter::TYPE));
            }
        );

        sort($keys);
        $counters = array();
        foreach ($keys as $key) {
            $raw = $this->redisCatchingErrors(
                function() use ($key) {
                    return $this->redis->hgetall($this->addPrefix($key));
                }
            );
            $counter = json_decode($raw['__meta'], true);
            unset($raw['__meta']);
            $counter['samples'] = array();
            foreach ($raw as $k => $value) {
                $counter['samples'][] = array(
                    'name' => $counter['name'],
                    'labelNames' => array(),
                    'labelValues' => json_decode($k, true),
                    'value' => $value
                );
            }
            usort($counter['samples'], function($a, $b){
                return strcmp(implode("", $a['labelValues']), implode("", $b['labelValues']));
            });
            $counters[] = $counter;
        }
        return $counters;
    }

    private function getRedisCommand($cmd)
    {
        switch ($cmd) {
            case Adapter::COMMAND_INCREMENT_INTEGER:
                return 'hIncrBy';
            case Adapter::COMMAND_INCREMENT_FLOAT:
                return 'hIncrByFloat';
            case Adapter::COMMAND_SET:
                return 'hSet';
            default:
                throw new \InvalidArgumentException("Unknown command");
        }
    }

    /**
     * @param array $data
     * @return string
     */
    private function toMetricKey(array $data)
    {
        return implode(':', array($data['type'], $data['name']));
    }

    private function addPrefix($key)
    {
        return self::PREFIX.$key;
    }

    /**
     * @param callable $callback
     * @return mixed
     */
    private function redisCatchingErrors(callable $callback)
    {
        $result = null;
        try {
            $result = call_user_func($callback);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
        return $result;
    }

}