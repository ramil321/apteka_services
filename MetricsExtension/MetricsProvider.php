<?php

namespace Apteka\MetricsExtension;

use Apteka\Application\AbstractApplicationExtensionProvider;
use Apteka\Application\Application;
use Apteka\MetricsExtension\Prometheus\PrometheusRedisAdapter;
use Apteka\MetricsExtension\Routing\MetricsPrimaryRoutesProvider;
use Apteka\RedisExtension\Connection\RedisConnectionPool;
use Apteka\Routing\Routing;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Prometheus\CollectorRegistry;

class MetricsProvider extends AbstractApplicationExtensionProvider
{
    /**
     * @inheritDoc
     */
    public function getName()
    {
        return 'Metrics';
    }

    /**
     * @param Application $app
     */
    public function register($app)
    {
        $this->registerServices($app);
        if ($app['container.composition'] === 'cli') {
            $this->customizePrimaryRouting($app);
        }
    }

    protected function registerServices(Application $app)
    {
        $app['metrics.logger'] = $app->share(
            function (Application $app) {
                $logger = new Logger('metrics');
                $logger->pushHandler(
                    new StreamHandler($app['paths']['log'] . '/metrics.log')
                );
                return $logger;
            }
        );

        $app['metrics.prometheus-adapter'] = $app->share(function (Application $app) {
            /**@var RedisConnectionPool $redisConnectionPool */
            $redisConnectionPool = $app['redis.connection-pool'];
            $connection = $redisConnectionPool->get('default');
            return new PrometheusRedisAdapter($connection, $app['metrics.logger']);
        });

        $app['metrics.prometheus-collector'] = $app->share(function (Application $app) {
            return new CollectorRegistry($app['metrics.prometheus-adapter']);
        });

        $app['metrics.registry'] = $app->share(function () {
            return new MetricsRegistry();
        });
    }

    protected function customizePrimaryRouting(Application $app)
    {
        if (! isset($app['routing'])) {
            return;
        }

        $app->extendShare('routing', function (Routing $routing) {
            $routing->addProvider(new MetricsPrimaryRoutesProvider($this->getPath()));
            return $routing;
        });
    }
}