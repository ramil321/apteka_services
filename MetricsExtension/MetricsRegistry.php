<?php


namespace Apteka\MetricsExtension;


class MetricsRegistry
{
    /**
     * @var MetricsInterface[]
     */
    private $metrics;

    /**
     * @param MetricsInterface $metric
     * @throws \Exception
     */
    public function register(MetricsInterface $metric)
    {
        $metricClass = get_class($metric);
        if ($this->has($metricClass)) {
            throw new \Exception(sprintf('Metric %s already exist', $metricClass));
        }
        $this->metrics[$metricClass] = $metric;
    }



    /**
     * @param $metricClass
     * @return MetricsInterface
     * @throws \Exception
     */
    public function get($metricClass)
    {
        if (! isset($this->metrics[$metricClass])) {
            throw new \Exception(sprintf('Metric %s not found', $metricClass));
        }

        return $this->metrics[$metricClass];
    }

    private function has($metricClass)
    {
        return isset($this->metrics[$metricClass]);
    }

    public function execute()
    {
        foreach ($this->metrics as $metric) {
            if ($metric->metricPage()) {
                $metric->execute();
            }
        }
    }
}