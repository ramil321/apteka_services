<?php

namespace Apteka\MetricsExtension;


interface MetricsInterface
{
    /**
     * @return string
     */
    public function getNameSpace();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getHelp();

    /**
     * @return array
     */
    public function getLabels();

    /**
     * @return boolean
     */
    public function metricPage();

    /**
     * @param array $options
     */
    public function execute(
        $options = []
    );

}