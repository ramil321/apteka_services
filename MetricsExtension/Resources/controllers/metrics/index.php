<?php

use Apteka\MetricsExtension\MetricsRegistry;
use Prometheus\CollectorRegistry as PrometheusCollectorRegistry;
use Prometheus\RenderTextFormat;

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

global $app;
/**@var MetricsRegistry $registry */
$registry = $app['metrics.registry'];
$registry->execute();

/**@var PrometheusCollectorRegistry $collectorRegistry */
$collectorRegistry = $app['metrics.prometheus-collector'];

$renderer = new RenderTextFormat();
$result = $renderer->render($collectorRegistry->getMetricFamilySamples());

header('Content-type: ' . RenderTextFormat::MIME_TYPE);

echo $result;

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
