<?php

namespace Apteka\CourseIntakeReminderExtension;

use Apteka\Application\AbstractExtensionProvider;
use Apteka\Application\Application;
use Apteka\CourseIntakeReminderExtension\CourseIntakeReminder\CourseIntakeReminderProductSubscription;
use Apteka\CourseIntakeReminderExtension\Helpers\CourseIntakeReminderOrderHelper;
use Apteka\CourseIntakeReminderExtension\Helpers\CourseIntakeReminderProductHelper;
use Apteka\CourseIntakeReminderExtension\Katren\ReminderHandler;
use Apteka\CourseIntakeReminderExtension\Notifications\CourseReminderExtensionNotificationAdapter;
use Apteka\Event\Dispatcher;
use Apteka\Katren\Import\HandlerFactory;
use Apteka\NotificationExtension\Integration\ParameterAdapters\DelegatingParametersAdapter;
use Apteka\CourseIntakeReminderExtension\Commands\ReminderCleanUpCommand;
use Symfony\Component\Console\Application as ConsoleApplication;

class CourseIntakeReminderProvider extends AbstractExtensionProvider
{
    /**
     * @inheritDoc
     */
    public function getName()
    {
        return 'course_intake_reminder';
    }

    public function register($app)
    {
        $this->registerServices($app);
        $this->customizeNotifications($app);
        $this->registerOnOrderAddListener($app, $app['event']);
        $this->customizeKatrenIntegration($app);
        $this->customizeConsole($app);
    }

    protected function registerServices(Application $app)
    {

        $app['course-intake-reminder.product-helper'] = $app->share(function(Application $app) {
            return new CourseIntakeReminderProductHelper(
                $app['dbc'],
                $app['dm']->repo('Branch'),
                $app['dm']->repo('Product'),
                $app['catalog.product-list-query-builder']
            );
        });

        $app['course-intake-reminder.order-helper'] = $app->share(function(Application $app) {
            return new CourseIntakeReminderOrderHelper(
                $app['dm']->repo('Order'),
                $app['sale.order.collection-factory'],
                $app['sale.cart.position.collection-factory']
            );
        });

        $app['course-intake-reminder.product-subscription'] = $app->share(function(Application $app) {
            return new CourseIntakeReminderProductSubscription(
                $app['dbc'],
                $app['mailer.user-subscription'],
                $app['mailer.subscription.types'],
                $app['course-intake-reminder.order-helper'],
                $app['course-intake-reminder.product-helper']
            );
        });
    }

    private function customizeNotifications(Application $app)
    {
        $app->extendShare(
            'notify.parameters-adapter',
            function (DelegatingParametersAdapter $adapter, Application $app) {
                $adapter->addAdapter(new CourseReminderExtensionNotificationAdapter());
                return $adapter;
            }
        );
    }

    private function registerOnOrderAddListener(Application $app, Dispatcher $event)
    {

        $event->listen('apteka:OnOrderComplete', function($orderId) use ($app) {
            /** @var $courseIntakeReminderProductSubscription CourseIntakeReminderProductSubscription*/
            $courseIntakeReminderProductSubscription = $app['course-intake-reminder.product-subscription'];
            if($_REQUEST['subscribe_course_products'] === 'Y'){
                $courseIntakeReminderProductSubscription->subscribeOrderProducts($orderId);
            }
            if($_REQUEST['subscribe_course_products'] === 'N'){
                $courseIntakeReminderProductSubscription->unsubscribeOrderProducts($orderId);
            }
        });
    }

    private function customizeKatrenIntegration(Application $app)
    {
        if (! isset($app['katren.import.factory.handler'])) {
            return;
        }

        $app->extendShare('katren.import.factory.handler', function(HandlerFactory $factory) {
            $factory->setDefinition('Reminder', ['class' => ReminderHandler::class]);
            return $factory;
        });
    }

    protected function customizeConsole(Application $app)
    {
        if (!isset($app['console'])) {
            return;
        }

        $app->extendShare('console', function (ConsoleApplication $console, Application $app) {
            $console->add(new ReminderCleanUpCommand($app));
            return $console;
        });
    }
}