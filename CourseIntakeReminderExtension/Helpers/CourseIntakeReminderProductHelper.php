<?php
namespace Apteka\CourseIntakeReminderExtension\Helpers;

use Apteka\Catalog\ProductListQueryBuilder;
use Apteka\Data\Object\Branch;
use Apteka\Data\Object\Product;
use Apteka\Data\Repo\Branch as BranchRepo;
use Apteka\Data\Repo\Product as ProductRepo;
use Apteka\Db\Connection;


class CourseIntakeReminderProductHelper
{
    /**
     * @var Connection
     */
    private $dbc;

    /**
     * @var callable
     */
    private $productRepo;

    /**
     * @var BranchRepo
     */
    private $branchRepo;

    /**
     * @var ProductListQueryBuilder
     */
    private $productListQueryBuilder;

    /**
     * @param Connection $dbc
     * @param BranchRepo $branchRepo
     * @param ProductRepo $productRepo
     * @param ProductListQueryBuilder $productListQueryBuilder
     */

    public function __construct(
        Connection $dbc,
        BranchRepo $branchRepo,
        ProductRepo $productRepo,
        $productListQueryBuilder
    ) {
        $this->dbc = $dbc;
        $this->branchRepo = $branchRepo;
        $this->productRepo = $productRepo;
        $this->productListQueryBuilder = $productListQueryBuilder;
    }
    /**
     * Ищет ид продуктов по sourceCode
     * @param  $sourceCode
     * @return array|null
     */
    public function findProductsIdsBySourceCode($sourceCode)
    {
        if(!$sourceCode){
            return null;
        }
        $query = $this->dbc
            ->createQuery()
            ->select('p.id')
            ->from('a_product','p')
            ->leftJoin('a_product_source', 's.id = p.source_id', 's')
            ->where('s.uid', $sourceCode)
        ;

        return $this->dbc->fetchColumn($query,'id');
    }

    /**
     * Проверяет наличие продукта в филиале и возвращяет его если он в наличии
     * @param int $productId
     * @param Branch $branch
     * @return Product|null
     * @throws \Exception
     */
    public function getBranchAvailableProduct($productId , Branch $branch)
    {
        $query = $this->productListQueryBuilder
            ->buildQuery([
                'branch' => $branch,
                'show_available' => true,
                'show_unavailable' => false
            ])
            ->select('e.ID')
            ->select('e.NAME')
            ->select('e.CODE')
            ->where('e.ID', $productId)
            ->limit(1)
        ;
        if($result = $this->dbc->fetchOne($query)) {
            $result['DETAIL_PAGE_URL'] = "/catalog/{$result['CODE']}/";
            return $this->productRepo->create($result);
        }else{
            return null;
        }
    }

    /**
     * Получить продукты по ид
     * @param array $productIds
     * @return Product[]
     */
    public function findProductsByIds(array $productIds)
    {
        if(!$productIds){
            return [];
        }
        return  $this->productRepo->findIndexedBy(['ID' => $productIds]);
    }
}