<?php
namespace Apteka\CourseIntakeReminderExtension\Helpers;

use Apteka\Data\Object\Branch;
use Apteka\Data\Repo\Order as OrderRepo;
use Apteka\Sale\Cart\PositionCollection;
use Apteka\Sale\OrderCollection\OrderCollection;


class CourseIntakeReminderOrderHelper
{
    /**
     * @var OrderRepo
     */
    private $orderRepo;

    /**
     * @var callable
     */
    private $positionCollectionFactory;


    /**
     * @param OrderRepo $orderRepo
     * @param callable $orderCollectionFactory
     * @param callable $positionCollectionFactory
     */
    public function __construct(
        OrderRepo $orderRepo,
        callable $orderCollectionFactory,
        callable $positionCollectionFactory
    ) {
        $this->orderRepo = $orderRepo;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->positionCollectionFactory = $positionCollectionFactory;
    }

    /**
     * Получает зазка по ид с позициями и продуктами
     * @var int $orderId
     * @return \Apteka\Data\Object\Order|null
     */
    public function getOrderById($orderId)
    {
        $order = $this->orderRepo->findOneBy(array('ID' => $orderId));
        if(!$order){
            return null;
        }
        /** @var OrderCollection $orderCollection */
        $orderCollection = call_user_func($this->orderCollectionFactory);
        $orderCollection->add($order);
        $orderCollection->attachUsers();
        $orderCollection->attachPositions();

        /** @var PositionCollection $positionCollection */
        $positionCollection = call_user_func($this->positionCollectionFactory);
        $positionCollection->addList($order->getPositions());
        $positionCollection->attachOrderPositions();
        $positionCollection->attachProducts();
        return $order;
    }

    /**
     * Получает зазка по uid с позициями
     * @var int $orderUid
     * @return \Apteka\Data\Object\Order|null
     */
    public function getOrderPositionsByOrderUid($orderUid)
    {
        if(!$orderUid){
            return null;
        }
        $orderId = $this->orderRepo->retrieveOrderIdByUid($orderUid);
        $order = $this->orderRepo->findOneBy(array('ID' => $orderId));
        if(!$order){
            return null;
        }
        /** @var OrderCollection $orderCollection */
        $orderCollection = call_user_func($this->orderCollectionFactory);
        $orderCollection->add($order);
        $orderCollection->attachPositions();

        return $order;
    }

    /**
     * Ищет Branch по Uid заказа
     * @param string $uid
     * @return Branch|null
     */
    public function findBranchByOrderUid($uid)
    {
        if(!$uid){
            return null;
        }
        $orderId = (int) $this->orderRepo->retrieveOrderIdByUid($uid);
        if($orderId) {
            $order = $this->orderRepo->findOneBy(array('ID' => $orderId));
            $order->getBranch();
            return $order->getBranch();
        }
    }

}