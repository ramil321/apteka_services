<?php


namespace Apteka\CourseIntakeReminderExtension\Commands;


use Apteka\Db\Connection;
use Apteka\Db\QueryBuilder;
use Apteka\Task\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class ReminderCleanUpCommand extends BaseCommand
{
    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('course-intake-reminder:clean-up');
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Connection $dbc */
        $dbc = $this->app['dbc'];

        $query = $dbc->createQuery()
            ->delete()
            ->from('a_course_intake_reminder')
            ->where('created_at', date_create()->modify('-1 month')->format('Y-m-d H:i:s'), QueryBuilder::LESS_THAN)
        ;
        $query->execute();
    }


}