<?php


namespace Apteka\CourseIntakeReminderExtension\Katren;


use Apteka\CourseIntakeReminderExtension\CourseIntakeReminder\CourseIntakeReminderProductSubscription;
use Apteka\CourseIntakeReminderExtension\Helpers\CourseIntakeReminderOrderHelper;
use Apteka\CourseIntakeReminderExtension\Helpers\CourseIntakeReminderProductHelper;
use Apteka\Data\Object\Product;
use Apteka\Data\Object\User;
use Apteka\DataManager\DataManager;
use Apteka\Db\Connection;
use Apteka\Katren\Import\Handler;
use Apteka\MailerExtension\Model\Subscription;
use Apteka\MailerExtension\Model\SubscriptionTypeOption;
use Apteka\MailerExtension\Subscription\SubscriptionTypes;
use Apteka\MailerExtension\Subscription\UserSubscription;
use Apteka\NotificationExtension\Integration\NewNotifyListener;
use Apteka\PushNotificationExtension\Integration\ListenerInterface;
use Apteka\Search\SphinxQuery;
use Apteka\Sender\Sender;
use Apteka\User\Exceptions\DeletedUserException;

class ReminderHandler extends Handler
{
    /**
     * @var DataManager
     */
    private $dm;

    public function initialize()
    {
        $this->dm = $this->container['dm'];
    }

    public function process()
    {
        DeletedUserException::throwIfPhoneDeleted($this->data['CustPhone']);

        /** @var \Apteka\Data\Repo\User $repoUsers */
        $repoUsers = $this->dm->repo('User');

        if(!$this->data['UID']){
            throw new \Exception('UID not found');
        }

        if(!$phone = $this->data['CustPhone']){
            throw new \Exception('Phone not found');
        }
        if(!$orderUid = $this->data['OrderUID']){
            throw new \Exception('OrderUID not found');
        }
        if(!$sourceId = $this->data['SourceUID']){
            throw new \Exception('SourceUID not found');
        }
        if(!$this->data['Message']){
            throw new \Exception('Message not found');
        }
        if(!$user = $repoUsers->findOneBy(array('LOGIN_EQUAL_EXACT' => $phone))){
            throw new \Exception('User '.$phone.' not found');
        }

        /** @var CourseIntakeReminderProductHelper $productHelper */
        $productHelper = $this->container['course-intake-reminder.product-helper'];
        /** @var CourseIntakeReminderOrderHelper $orderHelper */
        $orderHelper = $this->container['course-intake-reminder.order-helper'];
        /** @var CourseIntakeReminderProductSubscription $productSubscription */
        $productSubscription = $this->container['course-intake-reminder.product-subscription'];


        $userSubscriptions =  $this->getUserSubscription()->getUserSubscriptions($user->getId());
        $subscriptionTypes = $this->getSubscriptionTypes();
        $type = $subscriptionTypes->getByCode('course_intake_reminder');

        if(!$type){
            throw new \Exception('Subscription type not found');
        }
        if(!$userSubscriptionCourseIntakeReminder = $userSubscriptions['course_intake_reminder']){
            throw new \Exception('User subscription not found');
        }

        if(!$userSubscriptionCourseIntakeReminder->isSubscribed()){
            return;
        }

        $optionPush = $type->getOptionByCode('push');
        $optionMail = $type->getOptionByCode('email');

        if(!$optionPush){
            throw new \Exception('Subscription option push not found');
        }
        if(!$optionMail){
            throw new \Exception('Subscription option mail not found');
        }

        $productIds = $productHelper->findProductsIdsBySourceCode($this->data['SourceUID']);
        $subscribedProductId = null;
        $branch = null;
        if(count($productIds) == 1){
            $branch = $orderHelper->findBranchByOrderUid($this->data['OrderUID']);
            $subscribedProductId = $productIds[0];
        }elseif(count($productIds) > 1){
            $order = $orderHelper->getOrderPositionsByOrderUid($this->data['OrderUID']);
            $branch = $order->getBranch();
            foreach($order->getPositions() as $position){
                $positionData = $position->getData();
                if(in_array($positionData['PRODUCT_ID'], $productIds)){
                    $subscribedProductId = $positionData['PRODUCT_ID'];
                }
            }
        }

        if(!$branch){
            throw new \Exception('Branch not found');
        }

        if(!$subscribedProductId){
            throw new \Exception('Product for subscription not found');
        }

        if(!$productSubscription->checkUserSubscriptionToAnyProductIds($user->getId(), [$subscribedProductId])) {
            return;
        }
        if(!$availableProduct = $productHelper->getBranchAvailableProduct($subscribedProductId, $branch)) {
            return;
        }
        $this->sendNotification(
            $user,
            $userSubscriptionCourseIntakeReminder,
            $availableProduct,
            $optionPush,
            $optionMail
        );

    }


    /**
     * @return SubscriptionTypes
     */
    private function getSubscriptionTypes()
    {
        return $this->container['mailer.subscription.types'];
    }
    /**
     * @return UserSubscription
     */
    private function getUserSubscription()
    {
        return $this->container['mailer.user-subscription'];
    }

    /**
     * @return  ListenerInterface
     */
    private function getPushEventsHandler()
    {
        return $this->container['pushes.bitrix-events-handler'];
    }

    /**
     * @param Product $product
     * @return string
     */
    private function getProductAbsoluteUrl(Product $product)
    {
        $httpProtocol = $this->container['http.protocol'];
        $httpHost = $this->container['http.host'];
        return "$httpProtocol://$httpHost".$product->getUrl();
    }

    /**
     * @return  Sender
     */
    private function getSender()
    {
        return $this->container['sender'];
    }


    /**
     * @param User $user
     * @param Subscription $subscription
     * @param Product $product
     * @param SubscriptionTypeOption $optionPush
     * @param SubscriptionTypeOption $optionMail
     */
    private function sendNotification(
        User $user,
        Subscription $subscription,
        Product $product,
        SubscriptionTypeOption $optionPush,
        SubscriptionTypeOption $optionMail
    )
    {
        $this->dbc()->transaction(function () use ($user, $subscription, $product, $optionPush, $optionMail) {

            $find = $this->dbc()
                ->createQuery()
                ->forUpdate()
                ->select(1)
                ->from('a_course_intake_reminder')
                ->where('uid', $this->data['UID'])
                ->limit(1)
            ;

            if ($this->dbc()->fetchOne($find)) {
                return;
            }

            $typeMessageNames = ['ContinCrs' => 'продолжить', 'RepeatCrs' => 'повторить'];
            $themeMessage = ['ContinCrs' => 'Позаботьтесь о заказе следующей упаковки', 'RepeatCrs' => 'Повторить курс лечения?'];

            if($subscription->checkOptionActive($optionPush)){
                $this->getSender()
                    ->type('APTEKA_COURSE_INTAKE_REMINDER_PUSH')
                    ->values([
                        '__USER_ID' => $user->getId(),
                        'USER_ID' => $user->getId(),
                        'PRODUCT_ID' => $product->getId(),
                        'PRODUCT_NAME' => $product->getName(),
                        'PRODUCT_LINK' => $this->getProductAbsoluteUrl($product),
                        'TYPE' => $this->data['Message'],
                        'TYPE_NAME' => $typeMessageNames[$this->data['Message']]
                    ])
                    ->send();
            }
            if($subscription->checkOptionActive($optionMail)){
                $userData = $user->getData();
                if($email = $userData['EMAIL']){
                    $this->getSender()
                        ->type('APTEKA_COURSE_INTAKE_REMINDER_MAIL')
                        ->values([
                            'EMAIL' => $email,
                            'SUBJECT' => $themeMessage[$this->data['Message']],
                            'PRODUCT_NAME' => $product->getName(),
                            'PRODUCT_LINK' => $this->getProductAbsoluteUrl($product),
                            'TYPE_NAME' => $typeMessageNames[$this->data['Message']]
                        ])
                        ->send();
                }
            }

            $insert = $this->dbc()
                ->createQuery()
                ->insert('a_course_intake_reminder')
                ->set('uid',  $this->data['UID'])
                ->set('created_at', date_create()->format('Y-m-d H:i:s'));
            ;

            $this->dbc()->query($insert);

        });
    }

    /**
     * @return Connection
     */
    private function dbc()
    {
        return $this->container['dbc'];
    }
}