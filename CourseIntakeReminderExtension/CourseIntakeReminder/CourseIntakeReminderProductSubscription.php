<?php
namespace Apteka\CourseIntakeReminderExtension\CourseIntakeReminder;


use Apteka\CourseIntakeReminderExtension\Helpers\CourseIntakeReminderOrderHelper;
use Apteka\CourseIntakeReminderExtension\Helpers\CourseIntakeReminderProductHelper;
use Apteka\CourseIntakeReminderExtension\Model\CourseIntakeReminderUserProduct;
use Apteka\Db\Connection;
use Apteka\MailerExtension\Subscription\SubscriptionTypes;
use Apteka\MailerExtension\Subscription\UserSubscription;

class CourseIntakeReminderProductSubscription
{

    /**
     * @var array
     */
    private $cache = [];

    /**
     * @var Connection
     */
    private $dbc;

    /**
     * @var CourseIntakeReminderOrderHelper
     */
    private $courseIntakeReminderOrderHelper;

    /**
     * @var CourseIntakeReminderProductHelper
     */
    private $courseIntakeReminderProductHelper;

    /**
     * @var UserSubscription
     */
    private $userSubscription;

    /**
     * @var SubscriptionTypes
     */
    private $subscriptionTypes;


    /**
     * @param Connection $dbc
     * @param UserSubscription $userSubscription
     * @param SubscriptionTypes $subscriptionTypes
     * @param CourseIntakeReminderOrderHelper $courseIntakeReminderOrderHelper
     * @param CourseIntakeReminderProductHelper $courseIntakeReminderProductHelper
     */
    public function __construct(
        Connection $dbc,
        UserSubscription $userSubscription,
        SubscriptionTypes $subscriptionTypes,
        CourseIntakeReminderOrderHelper $courseIntakeReminderOrderHelper,
        CourseIntakeReminderProductHelper $courseIntakeReminderProductHelper
    ) {
        $this->dbc = $dbc;
        $this->userSubscription = $userSubscription;
        $this->subscriptionTypes = $subscriptionTypes;
        $this->courseIntakeReminderOrderHelper = $courseIntakeReminderOrderHelper;
        $this->courseIntakeReminderProductHelper = $courseIntakeReminderProductHelper;
    }

    /**
     * Подписать продукты заказа
     * @var int $orderId
     * @return null
     */
    public function subscribeOrderProducts($orderId)
    {
        $order = $this->courseIntakeReminderOrderHelper->getOrderById($orderId);
        if(!$order){
            return null;
        }
        $positions = $order->getPositions();
        $productIds = [];
        foreach($positions as $position){
            if($position->getProduct()->isCourse()){
                $productIds[] = $position->getProduct()->getId();
            }
        }
        $this->subscribeUserToProducts($order->getUser()->getId(), $productIds);
    }

    /**
     * Отписать продукты заказа
     * @var int $orderId
     */
    public function unsubscribeOrderProducts($orderId)
    {
        $order = $this->courseIntakeReminderOrderHelper->getOrderById($orderId);
        $positions = $order->getPositions();
        $productIds = [];
        foreach($positions as $position){
            if($position->getProduct()->isCourse()){
                $productIds[] = $position->getProduct()->getId();
            }
        }
        $this->unsubscribeUserToProducts($order->getUser()->getId(), $productIds);
    }


    private function saveUserSubscriptionToProduct($userId, array $productIds, $active)
    {
        if(!$userId){
            return null;
        }
        if(!$productIds){
            return null;
        }

        if($active){
            $type = $this->subscriptionTypes->getByCode('course_intake_reminder');
            if($type) {
                $this->userSubscription->subscribe($userId, $type);
                $optionsActive = [];
                foreach ($type->getOptions() as $option) {
                    $optionsActive[$option->getId()] = 1;
                }
                $this->userSubscription->setSubscriptionOptions($type, $userId, $optionsActive);
            }
        }
        $this->saveUserProductSubscription($userId, $productIds, $active);
    }

    /**
     * @param int $userId
     * @param array $productIds
     * @param boolean $active
     */
    private function saveUserProductSubscription($userId, array $productIds, $active)
    {
        if(!$userId){
            return;
        }
        if(!$productIds){
            return;
        }
        $optionsProduct = [];
        foreach($productIds as $productId){
            $optionsProduct[] = sprintf(
                '("%u", "%u", %u)',
                intval($userId),
                intval($productId),
                ($active ? 1 : 0)
            );
        }

        if($optionsProduct) {
            $query = sprintf(
                'INSERT INTO a_course_intake_user_product (user_id, product_id, active) ' .
                'VALUES %s ON DUPLICATE KEY UPDATE active = VALUES(active)',
                implode(', ', $optionsProduct)
            );
            $this->dbc->query($query);
        }
    }

    /**
     * Обновляет подписки пользователя на продукты
     * @param int $userId
     * @param array $productsActive
     */
    public function updateUserProductSubscription($userId, array $productsActive)
    {
        if(!$userId){
            return;
        }
        foreach($productsActive as $productSubscriptionId => $active) {
            $query = $this->dbc->createQuery()
                ->update('a_course_intake_user_product')
                ->set('active', $active)
                ->where('id', $productSubscriptionId)
                ->andWhere('user_id', $userId)
                ->limit(1)
            ;
            $this->dbc->query($query);
        }

    }

    /**
     * Отписывает пользователя от всех продуктов
     * @param int $userId
     */
    public function unsubscribeAllUserProducts($userId)
    {
        if(!$userId){
            return;
        }

        $query = $this->dbc->createQuery()
            ->update('a_course_intake_user_product')
            ->set('active', 0)
            ->where('user_id', $userId)
        ;
        $this->dbc->query($query);
    }

    /**
     * Подписывает пользователя на продукты
     * @param int $userId
     * @param array $productIds
     */
    public function subscribeUserToProducts($userId, array $productIds)
    {
        $this->saveUserSubscriptionToProduct($userId, $productIds, 1);
    }

    /**
     * Отписывает пользователя от продуктов
     * @param int $userId
     * @param array $productIds
     */
    public function unsubscribeUserToProducts($userId, array $productIds)
    {
        $this->saveUserSubscriptionToProduct($userId, $productIds, 0);
    }

    /**
     * Подгружаем подписки продуктов
     * @param int $userId
     * @return null|false
     */
    public function getUserProductSubscriptions($userId)
    {
        $cacheKey = 'products__user_id';
        if (! array_key_exists($cacheKey, $this->cache) || $this->cache[$cacheKey] != $userId) {
            $this->cache[$cacheKey] = $userId;
            $this->cache['products_subscriptions'] = $this->loadUserProductSubscriptions($userId);
        }

        return $this->cache['products_subscriptions'];
    }

    /**
     * Подгружаем подписки продуктов
     * @param int $userId
     * @return CourseIntakeReminderUserProduct[]
     */
    public function loadUserProductSubscriptions($userId)
    {
        if(!$userId){
            return null;
        }
        $queryOptionsProduct = $this->dbc->createQuery()
            ->select('*')
            ->from('a_course_intake_user_product')
            ->where('user_id', $userId)
            ->andWhere('active',1)
        ;

        $indexOptionsProduct = $this->dbc->fetchAll($queryOptionsProduct, 'product_id');
        $productIds = array_keys($indexOptionsProduct);
        if(!$productIds){
            return [];
        }
        $products = $this->courseIntakeReminderProductHelper->findProductsByIds($productIds);
        $courseIntakeReminderOptionProducts = [];
        foreach($indexOptionsProduct as $optionsProductRow){
            if($products[$optionsProductRow['product_id']]) {
                $courseIntakeReminderOptionProducts[] =
                    new CourseIntakeReminderUserProduct(
                        $optionsProductRow['id'],
                        $optionsProductRow['user_id'],
                        $products[$optionsProductRow['product_id']],
                        $optionsProductRow['active']
                    );
            }
        }
        return $courseIntakeReminderOptionProducts;
    }

    /**
     * Проверяет подписан ли пользователь на любой элемент из массива ид товаров
     * @param int $userId
     * @param array $productIds
     * @return boolean
     */
    public function checkUserSubscriptionToAnyProductIds($userId, array $productIds)
    {
        if(!$userId){
            return false;
        }
        if(!$productIds){
            return false;
        }
        $queryOptionsProduct = $this->dbc->createQuery()
            ->select('product_id')
            ->from('a_course_intake_user_product')
            ->whereIn('product_id', $productIds)
            ->andWhere('user_id', $userId)
            ->andWhere('active',1)
            ->limit(1)
        ;
        if($this->dbc->fetchOne($queryOptionsProduct)){
            return true;
        }else{
            return false;
        }
    }

}