<?php


namespace Apteka\CourseIntakeReminderExtension\Model;

use Apteka\Data\Object\Product;

class CourseIntakeReminderUserProduct
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var Product
     */
    private $product;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @param int $id
     * @param int $userId
     * @param boolean $active
     * @param Product $product
     */
    public function __construct($id, $userId, Product $product, $active)
    {
        $this->id = $id;
        $this->userId = $userId;
        $this->active = $active;
        $this->product = $product;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return null
     * @param $active boolean
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active ? true : false;
    }

}