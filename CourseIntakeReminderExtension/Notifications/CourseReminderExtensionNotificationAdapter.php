<?php


namespace Apteka\CourseIntakeReminderExtension\Notifications;


use Apteka\NotificationExtension\Integration\NotifyParametersAdapterInterface;
use Apteka\NotificationExtension\Notification\MessageType;

class CourseReminderExtensionNotificationAdapter implements NotifyParametersAdapterInterface
{
    public function adapt(MessageType $type, array $parameters)
    {
        return [
            '_notification_channels' => ['ui_course_intake_reminder'],
            '_lifetime' => '4 days',
            'product_id' => $parameters['PRODUCT_ID'],
            'type' => $parameters['TYPE']
        ];
    }

    public function support(MessageType $type)
    {
        return $type->getCode() === 'APTEKA_COURSE_INTAKE_REMINDER_PUSH';
    }
}