<?php
namespace Apteka\LeaderOfSalesExtension\LeaderOfSales;

use Apteka\Db\Connection;


class LeaderOfSales
{
    private $tableName = 'a_leader_of_sales';
    /**
     * @var Connection
     */
    private $dbc;

    public function __construct(Connection $dbc)
    {
        $this->dbc = $dbc;
    }

    /**
     * @param array $xmlIds
     * @return array
     * @throws \Apteka\Db\QueryExecutionException
     */
    public function getProductsFromXmlIds(array $xmlIds)
    {
        if (! $xmlIds) {
            return [];
        }
        $query = $this->dbc
            ->createQuery()
            ->select('ie.ID', 'id')
            ->select('ap.category_id','category_id')
            ->from('b_iblock_element','ie')
            ->leftJoin('a_product', 'ap.id = ie.ID','ap')
            ->whereIn('ie.XML_ID', $xmlIds);

        return $this->dbc->fetchAll($query);
    }

    /**
     * @param string $actionUuid
     * @param array $products
     * @param string $startDate
     * @param string $endDate
     * @throws \Apteka\Db\QueryExecutionException
     */
    public function insertLeaderOfSales($actionUuid, array $products, $startDate, $endDate)
    {
        $sqlValues = [];
        foreach ($products as $product) {
            if (! $product['category_id']) {
                continue;
            }
            $sqlValues[] = sprintf('("%s", %u, %u, "%s", "%s")',
                $this->dbc->escapeValue($actionUuid),
                $product['id'],
                $product['category_id'],
                $this->dbc->escapeValue($startDate),
                $this->dbc->escapeValue($endDate)
            );
        }

        $this->dbc->execute(sprintf(
            'INSERT INTO %s (action_uuid, product_id, category_id, start_date, end_date) VALUES %s ' .
            'ON DUPLICATE KEY UPDATE
                action_uuid = VALUES(action_uuid),
                category_id = VALUES(category_id),
                start_date = VALUES(start_date),
                end_date = VALUES(end_date)
            ',
            $this->tableName,
            implode(',', $sqlValues)
        ));
    }
}
