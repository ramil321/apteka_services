<?php


namespace Apteka\LeaderOfSalesExtension\Commands;


use Apteka\Db\Connection;
use Apteka\Db\QueryBuilder;
use Apteka\Task\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class LeaderOfSalesCleanUpCommand extends BaseCommand
{
    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('leader-of-sales:clean-up');
    }

    /**
     * @inheritDoc
     * @throws \Apteka\Db\QueryExecutionException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Connection $dbc */
        $dbc = $this->app['dbc'];

        $query = $dbc->createQuery()
            ->delete()
            ->from('a_leader_of_sales')
            ->where('end_date', date_create()->format('Y-m-d H:i:s'), QueryBuilder::LESS_THAN)
        ;
        $dbc->execute($query);
    }


}