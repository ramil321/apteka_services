<?php

namespace Apteka\LeaderOfSalesExtension\Migrations;

use Apteka\Db\Connection;
use Apteka\MigrationsExtension\Migrator\AbstractMigration;

class Migration20190905134349CreateLeaderOfSalesTable extends AbstractMigration
{

    public function getNumber()
    {
        return '20190905134349';
    }

    public function execute()
    {
        /** @var Connection $dbc */
        $dbc = $this->container['dbc'];

        $dbc->execute("
            CREATE TABLE IF NOT EXISTS a_leader_of_sales (
                action_uuid CHAR(36) NOT NULL COMMENT 'Uuid акции',
                product_id INT NOT NULL COMMENT 'ID товара',
                category_id INT UNSIGNED NOT NULL COMMENT 'ID категории',
                start_date DATETIME NOT NULL COMMENT 'Дата начала действия активности лидер продаж',
                end_date DATETIME NOT NULL COMMENT 'Дата окончания действия активности лидер продаж',
                PRIMARY KEY (product_id),
                KEY action_uuid__idx (action_uuid),
                KEY start_date__idx (start_date),
                KEY end_date__idx (end_date),
                FOREIGN KEY product_id__fk (product_id) REFERENCES b_iblock_element (ID) ON DELETE CASCADE ON UPDATE NO ACTION,
                FOREIGN KEY category_id__fk(category_id) REFERENCES a_product_category (id) ON DELETE CASCADE ON UPDATE NO ACTION
            ) ENGINE = InnoDB COMMENT 'Лидеры продаж'
        ");

    }


}

