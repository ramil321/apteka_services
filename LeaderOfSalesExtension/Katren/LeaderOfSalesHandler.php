<?php
namespace Apteka\LeaderOfSalesExtension\Katren;

use Apteka\DataManager\DataManager;
use Apteka\Katren\Import\Handler;
use Apteka\LeaderOfSalesExtension\LeaderOfSales\LeaderOfSales;
use DateTime;
use Exception;

class LeaderOfSalesHandler extends Handler
{
    /**
     * @var DataManager
     */
    private $dm;

    /**
     * @var LeaderOfSales
     */
    private $leaderOfSales;

    public function initialize()
    {
        $this->dm = $this->container['dm'];
        $this->leaderOfSales = $this->container['leader-of-sales'];
    }

    /**
     * @throws \Apteka\Db\QueryExecutionException
     * @throws Exception
     */
    public function process()
    {

        if (! $this->data['UID']) {
            throw new \Exception('uid not found');
        }

        if (! $this->data['startDate']) {
            throw new \Exception('startDate not found');
        }

        if (! $this->data['endDate']) {
            throw new \Exception('endDate not found');
        }

        if (new DateTime($this->data['startDate']) >= new DateTime($this->data['endDate'])) {
            throw new \Exception('bad date comparison');
        }

        if (! $this->data['Good']) {
            throw new \Exception('Good not found');
        }

        $xmlIds = [];
        foreach ($this->data['Good'] as $good) {
            $xmlIds[] = $good['UID'];
        }

        $products = $this->leaderOfSales->getProductsFromXmlIds($xmlIds);

        if (! $products) {
            throw new \Exception('Products not find');
        }

        $this->leaderOfSales->insertLeaderOfSales($this->data['UID'], $products, $this->data['startDate'], $this->data['endDate']);
        $this->cacheClear();
    }

    private function cacheClear()
    {
        /** @var \Apteka\Data\Repo\Product $repo */
        $repo = $this->dm->repo('Product');
        $GLOBALS['CACHE_MANAGER']->ClearByTag(sprintf('iblock_id_%u', $repo->getIBlockId()));
    }

}
