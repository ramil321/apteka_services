<?php
namespace Apteka\LeaderOfSalesExtension;

use Apteka\Application\AbstractExtensionProvider;
use Apteka\Application\Application;
use Apteka\Katren\Import\HandlerFactory;
use Apteka\LeaderOfSalesExtension\Commands\LeaderOfSalesCleanUpCommand;
use Apteka\LeaderOfSalesExtension\Katren\LeaderOfSalesHandler;
use Apteka\LeaderOfSalesExtension\LeaderOfSales\LeaderOfSales;
use Symfony\Component\Console\Application as ConsoleApplication;

class LeaderOfSalesProvider extends AbstractExtensionProvider
{
    public function getName()
    {
        return 'leader_of_sales';
    }

    public function register($app)
    {
        $this->registerServices($app);
        $this->customizeKatrenIntegration($app);
        $this->customizeConsole($app);
    }

    protected function registerServices(Application $app)
    {
        $app['leader-of-sales'] = $app->share(function(Application $app) {
            return new LeaderOfSales(
                $app['dbc']
            );
        });
    }

    private function customizeKatrenIntegration(Application $app)
    {
        if (! isset($app['katren.import.factory.handler'])) {
            return;
        }

        $app->extendShare('katren.import.factory.handler',
            function (HandlerFactory $factory) {
                $factory->setDefinition('LeaderOfSales', [
                    'class' => LeaderOfSalesHandler::class,
                ]);
                return $factory;
            }
        );
    }

    protected function customizeConsole(Application $app)
    {
        if (!isset($app['console'])) {
            return;
        }

        $app->extendShare('console', function (ConsoleApplication $console, Application $app) {
            $console->add(new LeaderOfSalesCleanUpCommand($app));
            return $console;
        });
    }
}
