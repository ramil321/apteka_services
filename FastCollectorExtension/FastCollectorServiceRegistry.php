<?php
namespace Apteka\FastCollectorExtension;

use Apteka\FastCollectorExtension\Interfaces\CollectorInterface;

class FastCollectorServiceRegistry
{
    /**
     * @var FastCollectorService[]
     */
    private $definitions = [];


    /**
     * @param $code
     * @param FastCollectorService $cachedCounterService
     * @return $this
     * @throws \Exception
     */
    public function add($code, FastCollectorService $cachedCounterService)
    {
        if ($this->has($code)) {
            throw new \Exception(sprintf('Definition %s already exist', $code));
        }

        $this->definitions[$code] = $cachedCounterService;

        return $this;
    }


    /**
     * @param $code
     * @return CollectorInterface
     * @throws \Exception
     */
    public function get($code)
    {
        if (! isset($this->definitions[$code])) {
            throw new \Exception(sprintf('Definition %s not found', $code));
        }

        return $this->definitions[$code]->getCollector();
    }

    public function has($code)
    {
        return isset($this->definitions[$code]);
    }



    /**
     * @return FastCollectorService[]
     */
    public function all()
    {
        return $this->definitions;
    }
}

