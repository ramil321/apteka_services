<?php


namespace Apteka\FastCollectorExtension\Commands;


use Apteka\FastCollectorExtension\FastCollectorServiceRegistry;
use Apteka\RedisExtension\Connection\RedisConnectionPool;
use Apteka\Task\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FastCollectorPersistCommand extends BaseCommand
{
    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('fast-collector:persist');
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var FastCollectorServiceRegistry $fastCollectorRegistry */
        $fastCollectorRegistry = $this->app['fast-collector.registry'];

        foreach ($fastCollectorRegistry->all() as $service) {
            $service->getPersister()->persist($service->getCollector());
        }
    }


}