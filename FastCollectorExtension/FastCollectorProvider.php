<?php


namespace Apteka\FastCollectorExtension;


use Apteka\Application\AbstractApplicationExtensionProvider;
use Apteka\Application\Application;
use Apteka\FastCollectorExtension\Commands\FastCollectorPersistCommand;
use Apteka\FastCollectorExtension\Redis\Counters\Article\RedisCounterCollectorArticle;
use Apteka\FastCollectorExtension\Redis\Counters\Article\RedisCounterPersisterArticle;
use Apteka\FastCollectorExtension\Redis\Counters\Product\RedisCounterCollectorProduct;
use Apteka\FastCollectorExtension\Redis\Counters\Product\RedisCounterPersisterProduct;
use Apteka\FastCollectorExtension\Redis\Insert\Article\RedisInsertCollectorArticle;
use Apteka\FastCollectorExtension\Redis\Insert\Article\RedisInsertPersisterArticle;
use Apteka\RedisExtension\Connection\RedisConnectionPool;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\Console\Application as ConsoleApplication;

class FastCollectorProvider extends AbstractApplicationExtensionProvider
{
    /**
     * @inheritDoc
     */
    public function getName()
    {
        return 'fast_collector_collection';
    }

    protected function registerServices(Application $app)
    {
        $app['fast-collector.registry'] = $app->share(function () {
            return new FastCollectorServiceRegistry();
        });

        $app['fast-collector.logger'] = $app->share(function (Application $app) {
            $logger = new Logger('fast-collector');
            $logger->pushHandler(new StreamHandler($app['paths']['log'] . '/fast-collector.logger.log'));
            return $logger;
        });

        $app->extendShare('fast-collector.registry', function(FastCollectorServiceRegistry $serviceRegistry, Application $app) {
            /**@var RedisConnectionPool $redisConnectionPool*/
            $redisConnectionPool = $app['redis.connection-pool'];
            $redisClient = $redisConnectionPool->get('default');
            $serviceRegistry->add(
                'ProductCounter',
                new FastCollectorService(
                    new RedisCounterCollectorProduct($redisClient),
                    new RedisCounterPersisterProduct($app['dbc'], $app['fast-collector.logger'])
                )
            );

            $serviceRegistry->add(
                'ArticleCounter',
                new FastCollectorService(
                    new RedisCounterCollectorArticle($redisClient),
                    new RedisCounterPersisterArticle($app['dbc'], $app['fast-collector.logger'])
                )
            );

            $serviceRegistry->add(
                'ArticleViewInsert',
                new FastCollectorService(
                    new RedisInsertCollectorArticle($redisClient),
                    new RedisInsertPersisterArticle($app['dbc'], $app['fast-collector.logger'])
                )
            );

            return $serviceRegistry;
        });

    }

    protected function customizeConsole(Application $app)
    {
        if (! isset($app['console'])) {
            return;
        }

        $app->extendShare('console', function(ConsoleApplication $console, Application $app) {
            $console->add(new FastCollectorPersistCommand($app));
            return $console;
        });
    }
}