<?php


namespace Apteka\FastCollectorExtension\Redis;


interface RedisPersisterInterface
{
    /**
     * @return int
     */
    public function getChunkSize();
}

