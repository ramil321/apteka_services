<?php


namespace Apteka\FastCollectorExtension\Redis\Counters\Product;


use Apteka\FastCollectorExtension\Redis\Counters\AbstractRedisCounterPersister;

class RedisCounterPersisterProduct extends AbstractRedisCounterPersister
{

    /**
     * @return int
     */
    public function getChunkSize()
    {
        return 1000;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return 'a_product_view_count';
    }

    /**
     * @return string
     */
    public function getTableFieldKey()
    {
        return 'product_id';
    }

    /**
     * @return string
     */
    public function getTableFiledValue()
    {
        return 'view_count';
    }

}