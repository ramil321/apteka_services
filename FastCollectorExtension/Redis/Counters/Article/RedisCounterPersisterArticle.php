<?php


namespace Apteka\FastCollectorExtension\Redis\Counters\Article;



use Apteka\FastCollectorExtension\Redis\Counters\AbstractRedisCounterPersister;

class RedisCounterPersisterArticle extends AbstractRedisCounterPersister
{

    /**
     * @return int
     */
    public function getChunkSize()
    {
        return 1000;
    }
    /**
     * @return string
     */
    public function getTableName()
    {
        return 'a_article_views_count';
    }

    /**
     * @return string
     */
    public function getTableFieldKey()
    {
        return 'article_id';
    }

    /**
     * @return string
     */
    public function getTableFiledValue()
    {
        return 'views_count';
    }
}