<?php


namespace Apteka\FastCollectorExtension\Redis\Counters;


use Apteka\FastCollectorExtension\Interfaces\CollectorInterface;
use Apteka\FastCollectorExtension\Redis\AbstractRedisCollector;
use Apteka\FastCollectorExtension\Redis\RedisInterface;
use Predis\Client;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractRedisCounterCollector extends AbstractRedisCollector
{
    /**
     * @param array $params
     */
    public function collect(array $params)
    {
        $optionsResolver = new OptionsResolver();
        $optionsResolver
            ->setOptional([
                'id'
            ])
            ->setAllowedTypes([
                'id' => ['int'],
            ])
        ;
        $options = $optionsResolver->resolve($params);
        $id = $options['id'];
        $this->redisClient->zincrby($this->getHashName(), 1, $id);
    }

    public function getAndResetAll($limit = 0)
    {
        $result = [];
        $this->redisClient->multi();
        $this->redisClient->zrange($this->getHashName(), 0, $limit-1, 'WITHSCORES');
        $this->redisClient->zremrangebyrank($this->getHashName(), 0, $limit-1);
        $data = $this->redisClient->exec();
        foreach(array_chunk($data[0], 2) as $chunk) {
            $result[$chunk[0]] = $chunk[1];
        }
        return $result;
    }

}