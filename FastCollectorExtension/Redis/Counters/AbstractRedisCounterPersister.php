<?php


namespace Apteka\FastCollectorExtension\Redis\Counters;


use Apteka\FastCollectorExtension\Interfaces\CollectorInterface;
use Apteka\FastCollectorExtension\Redis\AbstractRedisPersister;


abstract class AbstractRedisCounterPersister extends AbstractRedisPersister implements RedisCounterPersisterInterface
{
    /**
     * @param CollectorInterface $redisCollector
     */
    public function persist(CollectorInterface $redisCollector)
    {
        $this->loop($redisCollector, function($data) {

            if (! $data) {
                return;
            }

            $table = $this->getTableName();
            $tableFieldUniqueId = $this->getTableFieldKey();
            $tableFieldCounter = $this->getTableFiledValue();
            $insertValues = [];
            $log = [];

            foreach ($data as $id => $count) {

                if (!($id > 0 && $count > 0)) {
                    continue;
                }
                $log[] = [$id => $count];
                $insertValues[] = sprintf(
                    "(%u, %u)",
                    $id,
                    $count
                );
            }

            if (!$insertValues) {
                return;
            }
            $insertValuesSql = implode(', ', $insertValues);

            $updateCountersQuery = "
            INSERT INTO $table ($tableFieldUniqueId, $tableFieldCounter)
            VALUES $insertValuesSql
            ON DUPLICATE KEY UPDATE $tableFieldCounter = $tableFieldCounter + VALUES($tableFieldCounter)";

            $this->dbc->execute($updateCountersQuery);

            $this->logger->info('Redis counter persist table ' . $table, $log);
        });
    }

}