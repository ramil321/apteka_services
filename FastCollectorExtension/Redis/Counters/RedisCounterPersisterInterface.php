<?php


namespace Apteka\FastCollectorExtension\Redis\Counters;


interface RedisCounterPersisterInterface
{
    /**
     * @return string
     */
    public function getTableFieldKey();

    /**
     * @return string
     */
    public function getTableFiledValue();

    /**
     * @return string
     */
    public function getTableName();
}

