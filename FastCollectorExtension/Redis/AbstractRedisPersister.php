<?php


namespace Apteka\FastCollectorExtension\Redis;


use Apteka\FastCollectorExtension\Interfaces\CollectorInterface;
use Apteka\FastCollectorExtension\Interfaces\PersisterInterface;
use Apteka\Db\Connection;
use Psr\Log\LoggerInterface;


abstract class AbstractRedisPersister implements PersisterInterface, RedisPersisterInterface
{

    /**
     * @var Connection
     */
    protected $dbc;
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param Connection $dbc
     * @param LoggerInterface $logger
     */
    public function __construct(
        Connection $dbc,
        LoggerInterface $logger
    ){

        $this->dbc = $dbc;
        $this->logger = $logger;
    }

    protected function loop(CollectorInterface $redisCollector, callable $process)
    {
        do {
            $data = $redisCollector->getAndResetAll($this->getChunkSize());
            $dataSize = sizeof($data);
            $process($data);
        } while ($this->getChunkSize() <= $dataSize);
    }
}