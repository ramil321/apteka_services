<?php


namespace Apteka\FastCollectorExtension\Redis;


use Apteka\FastCollectorExtension\Interfaces\CollectorInterface;
use Apteka\FastCollectorExtension\Redis\RedisInterface;
use Predis\Client;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractRedisCollector implements CollectorInterface
{

    /**
     * @return string
     */
    public function getHashName()
    {
        return get_class($this);
    }

    /**
     * @var Client
     */
    protected $redisClient;

    /**
     * @param Client $redisClient]
     * @throws \Exception
     */
    public function __construct(
        Client $redisClient
    )
    {
        $this->redisClient = $redisClient;
    }
}