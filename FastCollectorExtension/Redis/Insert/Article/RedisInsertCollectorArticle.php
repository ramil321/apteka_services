<?php


namespace Apteka\FastCollectorExtension\Redis\Insert\Article;



use Apteka\FastCollectorExtension\Redis\AbstractRedisCollector;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RedisInsertCollectorArticle extends AbstractRedisCollector
{
    public function collect(array $params)
    {
        $optionsResolver = new OptionsResolver();
        $optionsResolver
            ->setRequired([
                'article_id'
            ])
            ->setAllowedTypes([
                'article_id' => ['int']
            ])
        ;
        $options = $optionsResolver->resolve($params);
        $options['viewed_at'] = date('Y-m-d H:i:s');
        $this->redisClient->rpush($this->getHashName(), json_encode($options));
    }

    public function getAndResetAll($limit = 0)
    {
        $this->redisClient->multi();
        $this->redisClient->lrange($this->getHashName(), 0, $limit-1);
        $this->redisClient->ltrim($this->getHashName(), $limit, -1);
        $data = $this->redisClient->exec();

        if (! $data[0]) {
            return [];
        }

        $data = array_map(function($value)
        {
            return json_decode($value, true);
        }, $data[0]);
        return $data;
    }
}