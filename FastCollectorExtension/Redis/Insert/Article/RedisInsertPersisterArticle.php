<?php


namespace Apteka\FastCollectorExtension\Redis\Insert\Article;



use Apteka\FastCollectorExtension\Interfaces\CollectorInterface;
use Apteka\FastCollectorExtension\Redis\AbstractRedisPersister;
use Apteka\FastCollectorExtension\Redis\Insert\AbstractRedisInsertPersister;

class RedisInsertPersisterArticle extends AbstractRedisPersister
{

    /**
     * @return int
     */
    public function getChunkSize()
    {
        return 1000;
    }

    /**
     * @return string
     */
    private function getTableName()
    {
        return 'a_article_rating_reviews';
    }

    /**
     * @return string
     */
    private function getTableFieldArticleId()
    {
        return 'article_id';
    }

    /**
     * @return string
     */
    private function getTableFiledViewedAt()
    {
        return 'viewed_at';
    }


    /**
     * @param CollectorInterface $redisCollector
     */
    public function persist(CollectorInterface $redisCollector)
    {
        $this->loop($redisCollector, function($data) {

            if (! $data) {
                return;
            }

            $table = $this->getTableName();
            $tableFieldKey = $this->getTableFieldArticleId();
            $tableFieldValue = $this->getTableFiledViewedAt();

            $insertValues = [];
            $log = [];
            foreach ($data as $value) {
                $insertValues[] = sprintf(
                    "(%u, '%s')",
                    $value[$this->getTableFieldArticleId()],
                    $value[$this->getTableFiledViewedAt()]
                );

                $log[$value[$this->getTableFieldArticleId()]] += 1;
            }

            if (! $insertValues) {
                return;
            }

            $insertValuesSql = implode(', ', $insertValues);

            $updateCountersQuery = "
                INSERT INTO $table ($tableFieldKey, $tableFieldValue)
                VALUES $insertValuesSql
            ";

            $this->dbc->execute($updateCountersQuery);
            $this->logger->info('Redis insert persist table '.$table, $log);

        });
    }

}