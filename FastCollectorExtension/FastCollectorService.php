<?php
namespace Apteka\FastCollectorExtension;

use Apteka\FastCollectorExtension\Interfaces\CollectorInterface;
use Apteka\FastCollectorExtension\Interfaces\PersisterInterface;

class FastCollectorService
{
    /**
     * @var CollectorInterface
     */
    private $collector;

    /**
     * @var PersisterInterface
     */
    private $persister;

    public function __construct(CollectorInterface $collector, PersisterInterface $persister)
    {
        $this->collector = $collector;
        $this->persister = $persister;
    }

    /**
     * @return CollectorInterface
     */
    public function getCollector()
    {
        return $this->collector;
    }

    /**
     * @return PersisterInterface
     */
    public function getPersister()
    {
        return $this->persister;
    }
}

