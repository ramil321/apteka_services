<?php


namespace Apteka\FastCollectorExtension\Interfaces;


interface CollectorInterface
{
    /**
     * @param array $params
     */
    public function collect(array $params);

    /**
     * @param int $limit
     * @return mixed
     */
    public function getAndResetAll($limit = 0);

}