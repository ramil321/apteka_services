<?php


namespace Apteka\FastCollectorExtension\Interfaces;


interface PersisterInterface
{
    /**
     * @param CollectorInterface $persister
     */
    public function persist(CollectorInterface $persister);

}
