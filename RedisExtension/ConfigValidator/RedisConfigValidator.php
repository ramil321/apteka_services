<?php


namespace Apteka\RedisExtension\ConfigValidator;


use Apteka\Config\Validator\AbstractConfigValidator;

class RedisConfigValidator extends AbstractConfigValidator
{
    /**
     * @inheritDoc
     */
    public function validate(array $config)
    {
        foreach ($config['redis']['connections'] as $value) {
            $this->assertNotEmptyValues($value, [
                'host',
                'port',
            ]);
        }
    }
}