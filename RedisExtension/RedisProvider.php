<?php


namespace Apteka\RedisExtension;


use Apteka\Application\AbstractApplicationExtensionProvider;
use Apteka\Application\Application;
use Apteka\Config\MainConfigValidator;
use Apteka\RedisExtension\ConfigValidator\RedisConfigValidator;
use Apteka\RedisExtension\Connection\RedisConnectionPool;
use Predis;

class RedisProvider extends AbstractApplicationExtensionProvider
{
    /**
     * @inheritDoc
     */
    public function getName()
    {
        return 'redis';
    }

    protected function registerServices(Application $app)
    {

        $app['redis.connection-pool'] = $app->share(function (Application $app) {
            $redisConnectionPool = new RedisConnectionPool();
            $nameSpace = $app['config']->get('redis/namespace');
            $connectionsCfg = $app['config']->get('redis/connections');

            foreach ($connectionsCfg as $key=>$cfg) {
                $client = new Predis\Client([
                    'scheme' => 'tcp',
                    'host' => $cfg['host'],
                    'port' => $cfg['port'],
                    'password' => $cfg['password']
                ], ['prefix' => $nameSpace.'_']);
                $redisConnectionPool->addConnection($key, $client);
            }

            return $redisConnectionPool;
        });

        $app->extendShare('config.validator', function (MainConfigValidator $validator, Application $app) {
            $validator->addValidator(new RedisConfigValidator($app['accessor']));
            return $validator;
        });
    }

}