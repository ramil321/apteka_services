<?php


namespace Apteka\RedisExtension\Connection;


use Predis\Client;

class RedisConnectionPool
{
    /**
     * @var Client[]
     *
     */
    private $clients;

    /**
     * @param string $name
     * @param Client $client
     * @return Client
     */
    public function addConnection($name, Client $client)
    {
        return $this->clients[$name] = $client;
    }

    /**
     * @param $name
     * @return Client
     * @throws \Exception
     */
    public function get($name)
    {
        if (! isset($this->clients[$name])) {
            throw new \Exception(sprintf('Client %s not found', $name));
        }

        return $this->clients[$name];
    }

}