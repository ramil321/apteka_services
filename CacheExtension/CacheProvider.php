<?php

namespace Apteka\CacheExtension;

use Apteka\Application\AbstractExtensionProvider;
use Apteka\Application\Application;
use Apteka\CacheExtension\Adapters\MemcacheAdapter;
use Apteka\CacheExtension\Commands\WarmupCommand;
use Apteka\CacheExtension\Warmup\WarmerPool;
use Bitrix\Main\Config\Configuration;
use Memcache;
use Symfony\Component\Console\Application as ConsoleApplication;

class CacheProvider extends AbstractExtensionProvider
{
    public function getName()
    {
        return 'cache';
    }

    /**
     * @param Application $app
     */
    public function register($app)
    {
        $this->registerServices($app);
        $this->customizeConsole($app);
    }

    private function registerServices(Application $app)
    {
        $cacheConfig = Configuration::getValue('cache');

        $app['memcache'] = $app->share(static function () use ($cacheConfig) {
            $host = $cacheConfig['memcache']['host'];
            $port = $cacheConfig['memcache']['port'];
            $memcache = null;

            if ($host && $port) {
                $memcache = new Memcache();
                $memcache->addServer($host, $port);
            }

            $memcache->setCompressThreshold(1000);

            return $memcache;
        });

        $app['cache'] = $app->share(static function (Application $app) use ($cacheConfig) {
            return new MemcacheAdapter($app['memcache'], $cacheConfig['sid']);
        });

        $app['warmer.pool'] = $app->share(static function () {
            return new WarmerPool();
        });
    }

    private function customizeConsole(Application $app)
    {
        if (!isset($app['console'])) {
            return;
        }

        $app->extendShare('console', static function (ConsoleApplication $console) use ($app) {
            $console->add(new WarmupCommand($app['warmer.pool']));

            return $console;
        });
    }
}
