<?php


namespace Apteka\CacheExtension\Warmup;


interface WarmerInterface
{
    public function getName();
    public function warmup();

    /**
     * @return boolean
     */
    public function isExpired();
}