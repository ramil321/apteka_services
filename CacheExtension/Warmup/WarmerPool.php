<?php

namespace Apteka\CacheExtension\Warmup;

use RuntimeException;

class WarmerPool
{
    /**
     * @var WarmerInterface[]
     */
    private $warmers;

    /**
     * @param WarmerInterface $warmer
     */
    public function add(WarmerInterface $warmer)
    {
        if (isset($this->warmers[$warmer->getName()])) {
            throw new RuntimeException(sprintf('Warmer %s already exists', $warmer->getName()));
        }

        $this->warmers[$warmer->getName()] = $warmer;
    }

    public function warmupAll()
    {
        foreach ($this->warmers as $warmer) {
            $warmer->warmup();
        }
    }

    public function warmupExpired()
    {
        foreach ($this->warmers as $warmer) {
            if ($warmer->isExpired()) {
                $warmer->warmup();
            }
        }
    }
}
