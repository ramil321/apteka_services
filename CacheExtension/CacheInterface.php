<?php

namespace Apteka\CacheExtension;

interface CacheInterface
{
    /**
     * @param string $key
     */
    public function get($key);

    /**
     * @param string $key
     * @param mixed $value
     * @param bool $compress
     * @param int $expire
     *
     * @return bool
     */
    public function set($key, $value, $compress = null, $expire = null);

    /**
     * @param string $key
     * @param mixed $value
     * @param bool $compress
     * @param int $expire
     *
     * @return bool
     */
    public function replace($key, $value, $compress = null, $expire = null);

    /**
     * @param string $key
     *
     * @return bool
     */
    public function delete($key);
}
