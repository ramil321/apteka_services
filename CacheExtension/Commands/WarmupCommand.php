<?php

namespace Apteka\CacheExtension\Commands;

use Apteka\CacheExtension\Warmup\WarmerPool;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class WarmupCommand extends Command
{
    private $pool;

    public function __construct(WarmerPool $pool)
    {
        $this->pool = $pool;
        parent::__construct('cache:warmup');
    }

    protected function configure()
    {
        $this->addOption('expired', 'e', InputOption::VALUE_NONE);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($input->getOption('expired')) {
            $this->pool->warmupExpired();
        } else {
            $this->pool->warmupAll();
        }
        $output->writeln('cache pool successfully updated');
    }
}
