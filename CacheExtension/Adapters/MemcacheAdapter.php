<?php

namespace Apteka\CacheExtension\Adapters;

use Apteka\CacheExtension\CacheInterface;
use Memcache;

class MemcacheAdapter implements CacheInterface
{
    private $memcache;
    private $sid;

    public function __construct(Memcache $memcache, $sid)
    {
        $this->memcache = $memcache;
        $this->sid = $sid;
    }

    public function get($key)
    {
        return $this->memcache->get($this->normalizeKey($key));
    }

    public function set($key, $value, $compress = false, $expire = null)
    {
        $flags = $compress ? MEMCACHE_COMPRESSED : null;

        return $this->memcache->set($this->normalizeKey($key), $value, $flags, $expire);
    }

    public function replace($key, $value, $compress = false, $expire = null)
    {
        $flags = $compress ? MEMCACHE_COMPRESSED : null;

        return $this->memcache->replace($this->normalizeKey($key), $value, $flags, $expire);
    }

    public function delete($key)
    {
        return $this->memcache->delete($this->normalizeKey($key));
    }

    private function normalizeKey($key)
    {
        return $key . $this->sid;
    }
}
